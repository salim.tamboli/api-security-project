package com.neosoft.poc2.convertor;

import org.springframework.stereotype.Component;

import com.neosoft.poc2.dto.StudentDTO;
import com.neosoft.poc2.model.Student;

@Component
public class StudentConvertor {
	
	public Student dtoToEntity(StudentDTO dto)
	{
		Student st=new Student();
		st.setFirstName(dto.getFirstName());
		st.setLastName(dto.getLastName());
		st.setEmailId(dto.getEmailId());
		st.setPhoneNumber(dto.getPhoneNumber());
		st.setStudentId(dto.getStudentId());
		
		return st;
	}
	public StudentDTO entityToDTO(Student student)
	{
		StudentDTO dto=new StudentDTO();
		dto.setFirstName(student.getFirstName());
		dto.setLastName(student.getLastName());
		dto.setEmailId(student.getEmailId());
		dto.setPhoneNumber(student.getPhoneNumber());
		dto.setStudentId(student.getStudentId());
		
		return dto;
		
	}

}
