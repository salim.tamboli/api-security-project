package com.neosoft.poc2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.poc2.model.Student;


@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>{

}
