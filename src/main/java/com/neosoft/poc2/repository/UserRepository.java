package com.neosoft.poc2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.poc2.model.User;



@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	User findByName(String name);

}
