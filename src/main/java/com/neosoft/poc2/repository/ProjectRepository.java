package com.neosoft.poc2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.poc2.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{

}
