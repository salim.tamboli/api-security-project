package com.neosoft.poc2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiWithSpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiWithSpringSecurityApplication.class, args);
	}

}
