package com.neosoft.poc2.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.neosoft.poc2.model.Student;

import com.neosoft.poc2.repository.StudentRepository;

import com.neosoft.poc2.service.StudentService;


@Service("studentService")
public class StudentServiceImpl implements StudentService{

	@Autowired
	StudentRepository studentRepository;
	
	@Override
	public Student save(Student student) {
		
		return studentRepository.save(student);
	}

	@Override
	public Student getStudentById(Integer id) {
		
		return studentRepository.getById(id);
	}
	

}
