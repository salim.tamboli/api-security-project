package com.neosoft.poc2.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.poc2.model.Project;
import com.neosoft.poc2.repository.ProjectRepository;
import com.neosoft.poc2.service.ProjectService;


@Service("prjectService")
public class ProjectServiceImpl implements ProjectService{

	@Autowired
	ProjectRepository projectRepository;
	
	@Override
	public Project save(Project project) {
		
		return projectRepository.save(project);
	}

	@Override
	public Project getProjectById(Integer Id) {
		
		return projectRepository.getById(Id);
	}
	

}
