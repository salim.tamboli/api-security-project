package com.neosoft.poc2.serviceImpl;


import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.neosoft.poc2.config.Encoder;
import com.neosoft.poc2.model.User;
import com.neosoft.poc2.repository.UserRepository;
import com.neosoft.poc2.service.UserService;



@Service
public class UserServiceImpl implements UserService,UserDetailsService{
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private Encoder encoder;

	

	@Override
	public User getUserByName(String username) throws UsernameNotFoundException {

		User user = userRepository.findByName(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return user;
	}



	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		 User user = userRepository.findByName(username);

	        if (user == null) {
	            throw new UsernameNotFoundException("User not found !!");
	        } else {
	        	Collection<SimpleGrantedAuthority> authority=new ArrayList<>();
	        	user.getRoles().forEach(role -> {
	        		authority.add(new SimpleGrantedAuthority(role.getRoleName()));
	        	});
	            return new org.springframework.security.core.userdetails.User(user.getName(), encoder.passwordEncoder().encode(user.getPassword()), authority);
	        }
	}

	

}
