package com.neosoft.poc2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.neosoft.poc2.dto.JwtRequest;
import com.neosoft.poc2.dto.JwtResponce;
import com.neosoft.poc2.JWTUtility.JwtUtill;
import com.neosoft.poc2.model.User;
import com.neosoft.poc2.service.UserService;



@RestController
public class JwtController {
	
	
	 @Autowired
	 private AuthenticationManager authenticationManager;


	    @Autowired
	    private UserService userService;

	    @Autowired
	    private JwtUtill jwtUtil;

	    @PostMapping(value = "/token")
	    public ResponseEntity<?> generateToken(@RequestBody JwtRequest jwtRequest) throws Exception {

	       
	        System.out.println(jwtRequest);
	        try {

	            this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(jwtRequest.getName(), jwtRequest.getPassword()));


	        } catch (UsernameNotFoundException e) {
	            e.printStackTrace();
	            throw new Exception("Bad Credentials");
	        }catch (BadCredentialsException e)
	        {
	            e.printStackTrace();
	            throw new Exception("Bad Credentials");
	        }


	        //fine area..
	        User user = userService.getUserByName(jwtRequest.getName());

	        String token = this.jwtUtil.generateToken(user);
	        System.out.println("JWT " + token);


	        return ResponseEntity.ok(new JwtResponce(token));

	    }

}
