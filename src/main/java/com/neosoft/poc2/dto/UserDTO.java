package com.neosoft.poc2.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	private Integer userId;
	private String password;
	private String userName;
	
}
