package com.neosoft.poc2.service;

import org.springframework.stereotype.Service;

import com.neosoft.poc2.model.Project;

@Service
public interface ProjectService {
	
	 Project save(Project project);
	
	 Project getProjectById(Integer Id);

}
