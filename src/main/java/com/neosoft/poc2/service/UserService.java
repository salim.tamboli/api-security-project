package com.neosoft.poc2.service;

import org.springframework.stereotype.Service;

import com.neosoft.poc2.model.User;


@Service
public interface UserService {
	
	public User getUserByName(String name);
	
	
	

}
