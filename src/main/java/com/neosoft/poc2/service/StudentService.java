package com.neosoft.poc2.service;
import org.springframework.stereotype.Service;

import com.neosoft.poc2.model.Student;


@Service
public interface StudentService {
	
	public Student save(Student student);
	
	public Student getStudentById(Integer id);

}
